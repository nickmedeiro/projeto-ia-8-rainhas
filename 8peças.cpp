#include <stdio.h>
#include <stdbool.h>
#include <list>
#include <string.h>
#include <iostream>
 
using namespace std;
 
typedef struct item{
    int number;
    bool isCorrect;
};
 
typedef struct coord{
    int x, y;
};
 
void initpuzzle (item puzzle[3][3]){
 
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
            puzzle[i][j].isCorrect = false;
        }
    }
 
    puzzle[0][0].number = 0;
    puzzle[0][1].number = 1;
    puzzle[0][2].number = 2;
    puzzle[1][0].number = 7;
    puzzle[1][1].number = 8;
    puzzle[1][2].number = 3;
    puzzle[2][0].number = 6;
    puzzle[2][1].number = 5;
    puzzle[2][2].number = 4;
}
 
bool verifyNumber(int posX, int posY, int number){
	
    if(number == 1){
        if(posX == 0 && posY == 0)
            return true;
    }else if(number == 2){
        if(posX == 0 && posY == 1)
            return true;
    }else if(number == 3){
        if(posX == 0 && posY == 2)
            return true;
    }else if(number == 4){
        if(posX == 1 && posY == 2)
            return true;
    }else if(number == 5){
        if(posX == 2 && posY == 2)
            return true;
    }else if(number == 6){
        if(posX == 2 && posY == 1)
            return true;
    }else if(number == 7){
        if(posX == 2 && posY == 0)
            return true;
    }else if(number == 8){
        if(posX == 1 && posY == 0)
            return true;
    }
    
    return false;
    
}
 
void addFila(list<coord> *fila, item puzzle[3][3]){
 
    coord curr = fila->front();
    coord toAdd;
 
    // pegar o elemento da direita
    if( (curr.y + 1) <= 2){
        // (x, y+1)
        if(puzzle[curr.x][curr.y + 1].number != 0){
            toAdd.x = curr.x;
            toAdd.y = curr.y + 1;
            fila->push_back(toAdd);
        }
    }
 
    // pegar o elemento de baixo
    if( (curr.x + 1) <= 2){
        // (x+1, y)
        if(puzzle[curr.x + 1][curr.y].number != 0){
            toAdd.x = curr.x + 1;
            toAdd.y = curr.y;
            fila->push_back(toAdd);
        }
    }
 
    // pegar elemento da esquerda
    if( (curr.y - 1) >= 0){
        // (x, y -1)
        if(puzzle[curr.x][curr.y - 1].number != 0){
            toAdd.x = curr.x;
            toAdd.y = curr.y - 1;
            fila->push_back(toAdd);
        }
    }
 
    // pegar o elemtno de cima de curr
    if( (curr.x - 1) >= 0 ){
        // (x-1, y)
        if(puzzle[curr.x - 1][curr.y].number != 0){
            toAdd.x = curr.x - 1;
            toAdd.y = curr.y;
            fila->push_back(toAdd);
        }
    }
}
 
void addPilha(list<coord> *pilha, item puzzle[3][3]){
 
    coord curr = pilha->back();
    pilha->pop_back();
    coord toAdd;
 
    // pegar o elemento de baixo
    if( (curr.x + 1) <= 2){
        // (x+1, y)
        if(puzzle[curr.x + 1][curr.y].number != 0){
            toAdd.x = curr.x + 1;
            toAdd.y = curr.y;
            pilha->push_back(toAdd);
        }
    }
 
    // pegar elemento da esquerda
    if( (curr.y - 1) >= 0){
        // (x, y -1)
        if(puzzle[curr.x][curr.y - 1].number != 0){
            toAdd.x = curr.x;
            toAdd.y = curr.y - 1;
            pilha->push_back(toAdd);
        }
    }
 
    // pegar o elemtno de cima de curr
    if( (curr.x - 1) >= 0 ){
        // (x-1, y)
        if(puzzle[curr.x - 1][curr.y].number != 0){
            toAdd.x = curr.x - 1;
            toAdd.y = curr.y;
            pilha->push_back(toAdd);
        }
    }
 
    // pegar o elemento da direita
    if( (curr.y + 1) <= 2){
        // (x, y+1)
        if(puzzle[curr.x][curr.y + 1].number != 0){
            toAdd.x = curr.x;
            toAdd.y = curr.y + 1;
            pilha->push_back(toAdd);
        }
    }
 
}
 
void printPuzzle(item puzzle[3][3]){
    for(int i = 0; i < 3; i++){
        for(int j = 0; j < 3; j++){
 
            if(puzzle[i][j].number == 0){
                printf("  ");
            }else{
                printf("%d ", puzzle[i][j].number);
            }
        }
        printf("\n");
    }
    printf("\n");
}
 
void printCaminho(char changes[100]){
 
    for(int i = 0; i < 100; i++){
 
        if(changes[i]=='c')
            printf(" Cima ");
 
        if(changes[i]=='d')
            printf(" Direita ");
 
        if(changes[i]=='b')
            printf(" Baixo ");
 
        if(changes[i]=='e')
            printf(" Esquerda ");
 
        if(changes[i+1]=='\0')
            break;
 
        printf("->");
 
    }
    
    printf("\n\n");
    
}
 
void solveInBFS (item puzzle[3][3], list<coord> fila){
 
    coord current;
    int aux;
    bool isChanged;
    string changedTo;
    char changes[100] = "";
 
    int iterator = 0;
 
    isChanged = false;
 
    while(!puzzle[1][0].isCorrect){
 
        iterator++;
        current.x = fila.front().x;
        current.y = fila.front().y;
        addFila( &fila, puzzle);
 
        if(puzzle[current.x][current.y].isCorrect == true){
            fila.pop_front();
            continue;
        }
 
        if(puzzle[current.x][current.y].number != 0){
 
            if(puzzle[current.x - 1][current.y].number == 0){
                if(current.x > 0){
                    aux = puzzle[current.x][current.y].number;
                    puzzle[current.x][current.y] = puzzle[current.x - 1][current.y];
                    puzzle[current.x - 1][current.y].number = aux;
                    puzzle[current.x - 1][current.y].isCorrect = verifyNumber((current.x - 1), current.y, aux);
                    changedTo = "CIMA";
                    strcat(changes, "c");
                    isChanged = true;
                }
            }else if(puzzle[current.x][current.y + 1].number == 0){
                if(current.y < 2){
                    aux = puzzle[current.x][current.y].number;
                    puzzle[current.x][current.y] = puzzle[current.x][current.y + 1];
                    puzzle[current.x][current.y + 1].number = aux;
                    puzzle[current.x][current.y + 1].isCorrect = verifyNumber(current.x, (current.y + 1), aux);
                    changedTo = "DIREITA";
                    strcat(changes, "d");
                    isChanged = true;
                }
            }else if(puzzle[current.x + 1][current.y].number == 0){
                if(current.x < 2){
                    aux = puzzle[current.x][current.y].number;
                    puzzle[current.x][current.y] = puzzle[current.x + 1][current.y];
                    puzzle[current.x + 1][current.y].number = aux;
                    puzzle[current.x][current.y - 1].isCorrect = verifyNumber((current.x + 1), current.y, aux);
                    changedTo = "BAIXO";
                    strcat(changes, "b");
                    isChanged = true;
                }
            }else if(puzzle[current.x][current.y - 1].number == 0){
                if(current.y > 0){
                    aux = puzzle[current.x][current.y].number;
                    puzzle[current.x][current.y] = puzzle[current.x][current.y  - 1];
                    puzzle[current.x][current.y - 1].number = aux;
                    puzzle[current.x][current.y - 1].isCorrect = verifyNumber(current.x, (current.y - 1), aux);
                    changedTo = "ESQUERDA";
                    strcat(changes, "e");
                    isChanged = true;
                }
            }
        }
 
        fila.pop_front();
        if(isChanged){
            cout<< aux << " FOI PARA " << changedTo << "\n" << endl;
            isChanged = false;
        }
        
        printPuzzle(puzzle);
        
    }
    
    printf("Numero total de iteracoes: %d \n\n", iterator);
    printCaminho(changes);
    
}
 
void solveInDFS (item puzzle[3][3], list<coord> pilha){
 
    coord current;
    int aux;
    bool isChanged;
    char changes[100] = "";
    bool aux2;
    string changedTo;
 
    isChanged = false;
 
    int iterator = 0;
 
    while(!puzzle[1][2].isCorrect){
 
        iterator++;
 
        current.x = pilha.back().x;
        current.y = pilha.back().y;
 
        if(pilha.size() == 0){
            current.x = 2;
            current.y = 2;
            pilha.push_back(current);
        }
 
        if(puzzle[current.x][current.y].isCorrect == true){
            pilha.pop_back();
            continue;
        }
 
        addPilha( &pilha, puzzle);
 
        if(puzzle[current.x][current.y].number != 0){
 
            if(puzzle[current.x - 1][current.y].number == 0){
                if(current.x > 0){
                    aux = puzzle[current.x][current.y].number;
                    puzzle[current.x][current.y] = puzzle[current.x - 1][current.y];
                    puzzle[current.x - 1][current.y].number = aux;
                    puzzle[current.x - 1][current.y].isCorrect = verifyNumber((current.x - 1), current.y, aux);
                    changedTo = "CIMA";
                    strcat(changes, "c");
                    isChanged = true;
                }
            }else if(puzzle[current.x][current.y + 1].number == 0){
                if(current.y < 2){
                    aux = puzzle[current.x][current.y].number;
                    puzzle[current.x][current.y] = puzzle[current.x][current.y + 1];
                    puzzle[current.x][current.y + 1].number = aux;
                    puzzle[current.x][current.y + 1].isCorrect = verifyNumber(current.x, (current.y + 1), aux);
                    changedTo = "DIREITA";
                    strcat(changes, "d");
                    isChanged = true;
                }
            }else if(puzzle[current.x + 1][current.y].number == 0){
                if(current.x < 2){
                    aux = puzzle[current.x][current.y].number;
                    puzzle[current.x][current.y] = puzzle[current.x + 1][current.y];
                    puzzle[current.x + 1][current.y].number = aux;
                    puzzle[current.x + 1][current.y].isCorrect = verifyNumber((current.x + 1), current.y, aux);
                    changedTo = "BAIXO";
                    strcat(changes, "b");
                    isChanged = true;
                }
            }else if(puzzle[current.x][current.y - 1].number == 0){
                if(current.y > 0){
                    aux = puzzle[current.x][current.y].number;
                    puzzle[current.x][current.y] = puzzle[current.x][current.y  - 1];
                    puzzle[current.x][current.y - 1].number = aux;
                    puzzle[current.x][current.y - 1].isCorrect = verifyNumber(current.x, (current.y - 1), aux);
                    changedTo = "ESQUERDA";
                    strcat(changes, "e");
                    isChanged = true;
                }
            }
        }
 
        if(isChanged){
            cout<< aux << " FOI PARA " << changedTo << "\n" << endl;
            isChanged = false;
        }
        
        printPuzzle(puzzle);
 
    }
    
    printf("Numero total de iteracoes: %d \n\n", iterator);
    printCaminho(changes);
    
}
 
int absolute(int x) {
    return x < 0 ? x * (-1) : x;
}
 
int verify(int x, int y, int n) {
	
    switch(n) {
 
        case 1:
            return x + y;
 
        case 2:
            return x + absolute(y - 1);
 
        case 3:
            return x + absolute(y - 2);
 
        case 4:
            return absolute(x - 1) + absolute(y - 2);
 
        case 5:
            return absolute(x - 2) + absolute(y - 2);
 
        case 6:
            return absolute(x - 2) + absolute(y - 1);
 
        case 7:
            return absolute(x - 2) + y;
 
        case 8:
            return absolute(x - 1) + y;
    }
    
    return 0;
    
}
 
int custoH1(item puzzle[3][3]) {
 
    int cost = 0;
 
    int i,j;
 
    for(i = 0; i < 3; i++)
        for(j = 0; j < 3; j++)
            if(verify(i,j,puzzle[i][j].number) != 0)
            cost += 1;
 
    return cost;
    
}
 
int custoH2(item puzzle[3][3]) {
 
    int cost = 0;
 
    int i,j;
    for(i = 0; i < 3; i++)
        for(j = 0; j < 3; j++)
            cost += verify(i,j,puzzle[i][j].number);
 
    return cost;
    
}
 
int escolheHeuristica(int s,item puzzle[3][3]){
	
    switch(s){
        case 1:
            return custoH1(puzzle);
        case 2:
            return custoH2(puzzle);
    }
    
}
 
int vericarJogada(int x, int y, int novoX, int novoY, item puzzle[3][3],int heuristica) {
 
    int cost;
    int tmp;
 
    tmp = puzzle[x][y].number;
    puzzle[x][y] = puzzle[novoX][novoY];
    puzzle[novoX][novoY].number = tmp;
 
    cost = escolheHeuristica(heuristica,puzzle);
 
    tmp = puzzle[x][y].number;
    puzzle[x][y] = puzzle[novoX][novoY];
    puzzle[novoX][novoY].number = tmp;
 
    return cost;
    
}
 
void buscaAEstrela(int x, int y,item puzzle[3][3],int heuristica){
	
    int melhorX = 0;
    int melhorY = 0;
    int melhorCost;
    int iterator = 0;
    char changes[100] = "";
    int cost = escolheHeuristica(heuristica,puzzle);
 
    while(cost > 0) {
 
        printf("\nCusto = %d\n", cost);
        printPuzzle(puzzle);
        printf("\n");
        //system("pause");
 
        melhorCost = 999999;
        int novoCost;
 
        if(x - 1 >= 0) {
            novoCost = vericarJogada(x, y, x - 1, y, puzzle,heuristica);
 
            if(novoCost < melhorCost) {
                melhorX = x - 1;
                melhorY = y;
                melhorCost = novoCost;
            }
        }
 
        if(y - 1 >= 0) {
            novoCost = vericarJogada(x, y, x, y - 1, puzzle,heuristica);
 
            if(novoCost < melhorCost) {
                melhorX = x;
                melhorY = y - 1;
                melhorCost = novoCost;
            }
        }
 
        if(x + 1 < 3) {
            novoCost = vericarJogada(x, y, x + 1, y, puzzle,heuristica);
 
            if(novoCost < melhorCost) {
                melhorX = x + 1;
                melhorY = y;
                melhorCost = novoCost;
            }
        }
 
        if(y + 1 < 3) {
            novoCost = vericarJogada(x, y, x, y + 1, puzzle,heuristica);
 
            if(novoCost < melhorCost) {
                melhorX = x;
                melhorY = y + 1;
                melhorCost = novoCost;
            }
        }
 
 
        int tmp = puzzle[x][y].number;
        puzzle[x][y] = puzzle[melhorX][melhorY];
        puzzle[melhorX][melhorY].number = tmp;
 
        if(x == melhorX - 1)
            strcat(changes, "c");
        else if(x == melhorX + 1){
            strcat(changes, "b");
        }
        else if(y == melhorY - 1){
            strcat(changes, "e");
        }
        else{
            strcat(changes, "d");
        }
 
        cost = melhorCost;
        x = melhorX;
        y = melhorY;
 
        iterator++;
        
    }
 
    printf("Custo = %d\n", cost);
    printPuzzle(puzzle);
    printf("\n");
    printf("Numero total de iteracoes: %d \n\n", iterator);
    printCaminho(changes);
    
}
 
int main(){
 
    list<coord> fila;
    list<coord> pilha;
    coord posInicial;
    posInicial.x = 0;
    posInicial.y = 0;
    fila.push_back(posInicial);
    posInicial.x = 2;
    posInicial.y = 2;
    pilha.push_back(posInicial);
    item puzzle[3][3];
    initpuzzle(puzzle);
    int busca;
    int heuristica;
 
    printf("Escolha uma busca para resolver o problema: \n1) para Busca em Largura; \n2) para Busca em Profundidade;  \n3) para Busca A*\n");
    scanf("%d", &busca);
 
    switch(busca){
        case 1:
            solveInBFS(puzzle, fila);
            break;
 
        case 2:
            solveInDFS(puzzle, pilha);
            break;
 
        case 3:
            printf("Escolha a heuristica a ser utilizada: \n1) para H1 ou\n2) para H2:");
            scanf("%d",&heuristica);
            buscaAEstrela(0,0,puzzle,heuristica);
    }
 
    fila.clear();
    pilha.clear();
    
    return 0;
    
}
